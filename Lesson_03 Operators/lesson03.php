<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1><?php echo "សួស្តី PHP7"; ?></h1>
    <h3><?php echo "ស្វាគមន៍ សិស្ស​ និស្សិតទាំងអស់គ្នា!"; ?></h3>

    <?php
        $txt1 = "Learn PHP";
        $txt2 = "W3Schools.com";
        $x = 5;
        $y = 4;

        echo "<h2>" . $txt1 . "</h2>";
        echo "Study PHP at " . $txt2 . "<br>";
        echo $x + $y;
        $sum=$x+$y;
        echo "<br>Result of Sum=$sum";
    ?>

    <h2>Using Print Method</h2>

    <?php
        print "<h2>PHP is Fun!</h2>";
        print "Hello world!<br>";
        print "I'm about to learn PHP!";
    ?>
<h2>Join String with Variable</h2>
    <?php
        $txt1 = "Learn PHP";
        $txt2 = "W3Schools.com";
        $x = 5;
        $y = 4;

        print "<h2>" . $txt1 . "</h2>";
        print "Study PHP at " . $txt2 . "<br>";
        print $x + $y;
        $sum2=$x+$y;

        print("<br>Result of Sum=$sum2");
    ?>

</body>
</html>