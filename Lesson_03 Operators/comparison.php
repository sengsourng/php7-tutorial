<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Using Comparison</title>
</head>
<body>
    <?php
        $x=30.00;
        $y=30.00;      

    ?>

    <h3>Using Equal (==) </h3>
    <?php 
        var_dump($x);
        var_dump($y);

        var_dump($x==$y); 
        if($x==$y){
            echo "x equal y";
        }else{
            echo "x not equal y !";
        }
    
    ?>

    <h3>Using Identical (===) </h3>
    <?php
    var_dump($x);
    var_dump($y);
    var_dump($x===$y);
    
    ?>


</body>
</html>