<?php
    class Fruit {
        // Properties
        public $name;
        public $color;
        public $price;

        function __construct($name=null,$color=null,$price=null) {
            $this->name = $name;
            $this->color = $color;
            $this->price = $price;
          }


        // Methods
        function set_name($name) {
            $this->name = $name;
        }
        function get_name() {
            return $this->name;
        }

        function set_color($color){
            $this->color=$color;
        }
        function get_color(){
            return $this->color;
        }

        function set_price($price){
            $this->price=$price;
        }

        function get_price(){
            return $this->price;
        }

        public function displayFruit()
        {
        return ("Name :".$this->name. "- Color :". $this->color. "- Price :". $this->price);
        }

    }



    // Create object

    $apple = new Fruit("Apple","Red",16);
    // $apple->set_name('Apple');
    // $apple->set_color('Red');
    // $apple->set_price(15);

    $babana=new Fruit("Banana","Orange",23);
    // $babana->set_name('Banana');
    // $babana->set_color('Orange');
    // $babana->set_price(20);

    $mango=new Fruit();
    $mango->set_name('Mango');
    $mango->set_color('Orange');
    $mango->set_price(26);
   
?>








<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<div class="container mt-3">
  <h2>Fruit Information</h2>
  <p>List all Fuits</p>            
  <table class="table">
    <thead>
      <tr>
        <th>Fruit Name</th>
        <th>Color</th>
        <th>Price</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><?php echo $apple->get_name(); ?></td>
        <td><?php echo $apple->get_color(); ?></td>
        <td><?php echo $apple->get_price(); ?> USD</td>
      </tr>

      <tr>
        <td><?php echo $babana->get_name(); ?></td>
        <td><?php echo $babana->get_color(); ?></td>
        <td><?php echo $babana->get_price(); ?> USD</td>
      </tr>

      <tr>
        <td><?php echo $mango->get_name(); ?></td>
        <td><?php echo $mango->get_color(); ?></td>
        <td><?php echo $mango->get_price(); ?> USD</td>
      </tr>
      
    </tbody>
  </table>
</div>

</body>
</html>
