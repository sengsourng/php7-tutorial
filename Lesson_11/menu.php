<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <div class="container-fluid">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link active" href="home.php">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?page=news">News</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?page=life">Life</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="contact.php">Contact</a>
      </li>
    </ul>
  </div>
</nav>