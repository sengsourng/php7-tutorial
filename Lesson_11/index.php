<!-- Header -->
<?php
    include_once('header.php');
?>

<!-- Menu -->
<?php
    include_once('menu.php');
?>

<div class="container mt-5">
  <?php
    $page="";
    if(isset($_GET['page'])){
        $page=$_GET['page'];
        include_once($page.".php");
    }        
  ?>
</div>




<!-- Footer -->

<?php
    include_once('footer.php');
?>