<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<div class="container mt-3">
  <h3>Upload image</h3>

  <form action="upload.php" method="post" enctype="multipart/form-data">
    
    <div class="input-group mb-3">    
        <input type="file" class="form-control" placeholder="Search" 
        name="fileToUpload" id="fileToUpload">
        <button class="btn btn-success" type="submit">Upload</button> 
    </div>

    </form>
  
</div>

</body>
</html>
