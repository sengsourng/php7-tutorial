<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ការប្រើប្រាស់ String</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Angkor&family=Hanuman:wght@400;700&display=swap" rel="stylesheet">

<!-- font-family: 'Angkor', cursive;
font-family: 'Hanuman', serif; -->
<style>
    h1,h2,h3{
        font-family: 'Angkor';
    }
    h4{
        font-family: 'Hanuman';
        font-size: 24px;
    }
    p{
        font-family: 'Hanuman';
        font-size: 22px;
    }
</style>

</head>
<body>
    <h4>ប្រើប្រាស់ ប្រវែង String String Lenght(strlen())</h4>
    <?php
        echo strlen("Hello world!"); // outputs 12

        $first_name="សេង";
        $last_name="ស៊ង់";
        $full_name=$first_name ." ". $last_name;

        echo "<p> ឈ្មោះរបស់អ្នកគឺ : ". $full_name ." មានប្រវែង ". strlen($full_name) ."</p>";
    ?>

    <h4>រាប់ពាក្យក្នុង​ String</h4>
    <?php
        $full_name_latin="SENG Sourng";
        
        echo "<p> ឈ្មោះ SENG Sourng គឺ​មានចំនួន ".str_word_count($full_name_latin)." ពាក្យ</p>"; // outputs 2

        $str="I want to study ICT";
        echo "<p> ឃ្លា : ".$str." គឹមានចំនួន ".str_word_count($str) ." ពាក្យ</p>";
    ?>
</body>
</html>