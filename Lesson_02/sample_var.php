<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Testing Variable</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Angkor&family=Hanuman:wght@400;700&display=swap" rel="stylesheet">

<!-- font-family: 'Angkor', cursive;
font-family: 'Hanuman', serif; -->
<style>
    h1,h2,h3{
        font-family: 'Angkor';
    }
    h4{
        font-family: 'Hanuman';
        font-size: 24px;
    }
    p{
        font-family: 'Hanuman';
        font-size: 22px;
    }
</style>

</head>
<body>
    <h1>សាកល្បងប្រើ Varable</h1>
    <h4>ប្រភេទទិន្ន​ន័យជា String</h4>

    <?php
        $x = "<p>នេះគឺជា String !</p>";
        $y = 'Hello world!';

        echo $x;
        echo "<br>";
        echo $y;

        var_dump($x);
        echo "<br>";
        var_dump($y);
    ?>

    <h4>ប្រើប្រាស់ ចំនួនគត់ (Integer)</h4>
    <?php
        $x = 5985;
        var_dump($x);
    ?>

    <h4>ប្រើប្រាស់ ចំនួនទស្សភាគ(Floating Point or Decimal)</h4>
    <?php
        $x = 10.365;
        var_dump($x);
    ?>

    <h4>ការប្រើប្រាស់ Boolean</h4>
    <?php 
        $x = true;
        $y = false;

        var_dump($x);
    ?>

    <h4>ការប្រើប្រាស់ Array(PHP Array)</h4>
    <?php 
        $cars = array("Volvo","BMW","Toyota");
        var_dump($cars);        
    ?>
    
  
    <?php 
        for($i=0;$i<count($cars);$i++){
            ?>
                <h4><?php echo $cars[$i]; ?></h4>
            <?php
        }
    ?>


    <h4>ការប្រើប្រាស់ Object</h4>

    <?php
        class Car {
            public $color;
            public $model;

            public function __construct($color, $model) {
                $this->color = $color;
                $this->model = $model;
            }

            public function message() {
                $color=$this->color;
                return '<p style=color:'. $color .'>'.'My car is a  ' . $this->color . ' ' . $this->model . '!</p>';
            }
        }

            //បង្កើត​ Object
            $myCar = new Car("blue", "Volvo");
            echo $myCar -> message(); // ហៅ​ function/ method message() មកប្រើ
            echo "<br>";

            $myCar = new Car("green", "Toyota");
            echo $myCar -> message();
        ?>

        <h4>ប្រើប្រាស់ NULL</h4>
        <?php
            $x = "Hello world!";
            $x = null;
            var_dump($x);
        ?>
</body>
</html>