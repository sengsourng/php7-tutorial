<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Result from Form Post</title>
</head>
<body>
    <h2>Result of User Enter Information</h2>
    Welcome <?php echo $_POST["full_name"]; ?><br>
    Your email address is: <?php echo $_POST["email"]; ?>
    <br>
    <a href="form_post.php">Back To Form</a>
</body>
</html>