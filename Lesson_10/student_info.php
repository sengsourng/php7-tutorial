<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>


    <?php
        // define variables and set to empty values
        $name = $email = $gender = $comment = $website = "";

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $name = test_input($_POST["full_name"]);
            $email = test_input($_POST["email"]);
            $website = test_input($_POST["website"]);
            $comment = test_input($_POST["comment"]);
            if(isset($_POST["gender"])){
                $gender = test_input($_POST["gender"]);   
            }else{
                $gender="";
            }
        }

        function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }
    ?>

<div class="container mt-3">
  <h2>Register Student Info</h2>
  <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"> 
    <div class="mb-3 mt-3">
      <label for="full_name">Full Name:</label>
      <input type="text" class="form-control" id="full_name" placeholder="Enter Full Name" name="full_name">
    </div>

    <div class="mb-3 mt-3">
      <label for="email">Email:</label>
      <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
    </div>

    <div class="mb-3 mt-3">
      <label for="website">Website:</label>
      <input type="text" class="form-control" id="website" placeholder="Enter website" name="website">
    </div>

    <div class="mb-3 mt-3">
        <label for="comment">Comments:</label>
        <textarea class="form-control" rows="5" id="comment" name="comment"></textarea>
      
    </div>


    <div class="mb-3 mt-3">
        <label for="gender">Gender:</label>
        <div class="form-check">
            <input type="radio" class="form-check-input" id="gender" name="gender" value="Male" checked>Male            
        </div>
        <div class="form-check">
            <input type="radio" class="form-check-input" id="gender" name="gender" value="Female" checked>Female            
        </div>
        <div class="form-check">
            <input type="radio" class="form-check-input" id="gender" name="gender" value="Other" checked>Other            
        </div>      
    </div>


    
    
    <button type="submit" class="btn btn-primary">Register</button>
  </form>
</div>


<?php
    echo "<h2>Your Input:</h2>";
    echo $name;
    echo "<br>";
    echo $email;
    echo "<br>";
    echo $website;
    echo "<br>";
    echo $comment;
    echo "<br>";
    echo $gender;
   
?>
</body>
</html>
